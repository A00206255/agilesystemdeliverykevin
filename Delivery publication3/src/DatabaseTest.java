import java.sql.SQLException;
import java.text.ParseException;

import junit.framework.TestCase;

public class DatabaseTest extends TestCase 
{
		//Test No. 1
		//Objective: To test entry of customer data
		//Input(s): First Name = "Kevin", Surname = "McQuaide", Address = "Longford", Phone Number = "0851181958"
		//Expected Output: True
	public void testCustomerEntry001() throws SQLException {
		Database testObject = new Database();
		assertEquals(true, testObject.addNewCustomer("Kevin", "McQuaide", "Longford", "0851181958"));// Will check the expected output "true" matches the
																									// output from the method
	}

	//Test No. 2
	//Objective: To test entry of delivery person data
	//Input(s): First Name = "Walter", Surname = "White", Area = "Athone" 
	//Expected Output: True
	public void testDeliveryPersonEntry001() {
		Database testObject = new Database();
		assertEquals(true, testObject.addNewDelivery("Walter", "White", "Athlone"));		// Will check the expected output "true" matches the
																									// output from the method
	}
	
	//Test No. 3
	//Objective: To test entry of subscription data
	//Input(s): Holiday Start = "13/07/2018", Holiday End = "20/07/2018" 
	//Expected Output: True
	public void testSubscriptionEntry001() throws SQLException, ParseException {
		Database testObject = new Database();
		assertEquals(true, testObject.addNewSubscription("13/07/2018", "20/07/2018", false, false, false, false));		// Will check the expected output "true" matches the
																									// output from the method
	}
	
	//Test No. 4
	//Objective: To test entry of docket
	//Input(s): Area = "Dublin", Delivery person first name = "Jesse", Delivery person surname = "Pinkman", Customer first name "Saul", Cust surname "Goodman", sub id 1
	//Expected Output: True
	public void testDocketEntry001() throws SQLException, ParseException {
		Database testObject = new Database();
		assertEquals(true, testObject.addNewDocket("Dublin", "Jesse", "Pinkman", "Saul", "Goodman", 1));		// Will check the expected output "true" matches the
		// output from the method
	}
}