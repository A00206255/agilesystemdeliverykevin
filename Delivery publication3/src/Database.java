import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.io.PrintWriter;


import javax.swing.JOptionPane;

public class Database 
{
	/** Database Connection */
    private Connection con;
    
    /** Used for executing database statements*/
    private Statement stmt;      
    
    /** Holds result of stmt, moves to next row in database*/
    private ResultSet rs;
    
    /** Holds the number of rows in database value*/
    int count;
    
    /** Holds the current number of the row in the database we are on*/
    int current;
	
	/*Customer values*/
    private String cust_first_name;
	private String cust_surname;
	private String cust_addr;
	private String cust_phone;

	/*Delivery person values*/
	private String del_first_name;
	private String del_surname;
	private String del_area;

	/*Subscription values*/
	private String date_start;
	private String date_end;
	private boolean independent;
	private boolean mirror;
	private boolean leader;
	private boolean topic;
	
	/*Delivery docket values*/
	private String docket_delarea;
	private String docket_delivery_firstName;
	private String docket_delivery_surname;
	private String docket_cust_firstName;
	private String docket_cust_surname;
	private int docket_sub_id;
	
    public Database()
    {
        con = null;         /** @param con       the connection parameter to connect to MS Access database.*/
        stmt = null;        /** @param stmt      the statement parameter to execute database statements.*/
        rs = null;          /** @param rs        the result parameter to hold the result of statement and move rows.*/
        count = 0;          /** @param count     the int value to count number of rows in database.*/
        current = 0;        /** @param current   the int value to keep track of which row in database we are on.*/
    
        dbConn();// method to connect to database using odbc-jdbc
      //  initDB();// method to initialise gui with database info

    }
    
    private void initDB() {
		// TODO Auto-generated method stub
		
	}

	public void dbConn()
    {
		try		
		{
			// driver to use with named database which is stored alongside program in C Drive. Access database must be in this directory
			//Requires ucanaccess library to be implemented to connect
	        //String url = "jdbc:ucanaccess://c:/Agile/Newsagent.accdb";
			String url = "jdbc:ucanaccess://src/Newsagent.accdb";
	        
	        // connection represents a session with a specific database
	        con = DriverManager.getConnection(url);
	
	        // stmt used for executing sql statements and obtaining results
	        stmt = con.createStatement();
	        System.out.println("Connected");
	        rs = stmt.executeQuery("SELECT * FROM Customer");
	
			while(rs.next())	// count number of rows in table
	        {
	            count++;
	        }
	        System.out.println(count);
	        rs.close();
		}
		catch(Exception e) 
		{
			System.out.println("Unable to connect to database");
			/*JOptionPane.showMessageDialog(database.this,"Error in startup.","Error", JOptionPane.PLAIN_MESSAGE);} */
		}
    }
	
	/*Method called when creating new customer to insert new row*/
	public boolean addNewCustomer(String fname, String sname, String addr, String phone) throws SQLException 
	{
		
		this.cust_first_name = fname;
		this.cust_surname = sname;
		this.cust_addr = addr;
		this.cust_phone = phone;

		String checkNum = "Select Cust_PhoneNo From Customer";
		rs = stmt.executeQuery(checkNum);
		
		while(rs.next())
		{
			if(cust_phone.equals(rs.getString("Cust_PhoneNo")))
			{
				JOptionPane.showMessageDialog(null, "Customer already exists");
				return false;
			}
		}
		
        String newCust = "INSERT INTO Customer(Cust_FirstName, Cust_Surname, Cust_PhoneNo, Cust_Address)VALUES('"+cust_first_name+"', '"+cust_surname+"', '"+cust_phone+"', '"+cust_addr+"')";
        
    try 
        {
			stmt.executeUpdate(newCust);
	        System.out.println("Customer Insert Completed");
		
			return true;
}
        catch (SQLException e) 
        {
			// TODO Auto-generated catch block 
			e.printStackTrace();
			return false;
		}
	}
	
	public void readCustTable() throws SQLException
	{
		rs = stmt.executeQuery("SELECT * FROM Customer");
		
        rs.next();   
        
        String fname;                  /** @param name          the String to store the name from the database.*/
        String sname;              /** @param address1      the String to store the first address line from the database*/
        
        fname = rs.getString("Cust_FirstName");
        sname = rs.getString("Cust_Surname");
        System.out.println("First Name: " +fname);
        System.out.println("Surname : " +sname);
        
/*		while(rs.next())	// count number of rows in table
        {
            count++;
        }
        System.out.println(count);*/
        rs.close();
	}
	
		

    /*Method called when creating new delivery person to insert new row*/
	public boolean addNewDelivery(String fname, String sname, String area)
	{
		this.del_first_name = fname;
		this.del_surname = sname;
		this.del_area = area;
		
        String newDelivery = "INSERT INTO Delivery_Person(First_Name, Surname, Delivery_Area) VALUES('"+del_first_name+"', '"+del_surname+"', '"+del_area+"')";
        
        try 
        {
			stmt.executeUpdate(newDelivery);
	        System.out.println("Delivery Person Insert Completed");
			return true;
		}
        catch (SQLException e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

    /*Method called when creating new subscription to insert new row*/
	public boolean addNewSubscription(String dateStart, String dateEnd, boolean independent, boolean mirror, boolean leader, boolean topic) throws SQLException, ParseException
	{
		this.date_start = dateStart;
		this.date_end = dateEnd;
		this.independent = independent;
		this.mirror = mirror;
		this.leader = leader;
		this.topic = topic;
		
		String i = null, m = null, l = null, t = null;
		
		if(independent == true)
		{
			i = "Yes";
		}
		else if (independent == false)
		{
			i = "No";
		}
		
		if(mirror == true)
		{
			m = "Yes";
		}
		else if (mirror == false)
		{
			m = "No";
		}
		
		if(leader == true)
		{
			l = "Yes";
		}
		else if (leader == false)
		{
			l = "No";
		}
		if(topic == true)
		{
			t = "Yes";
		}
		else if (topic == false)
		{
			t = "No";
		}
		
		
		rs = stmt.executeQuery("Select max(Cust_ID) From Customer");
		
		int recentCustId = 0;
		
		if (rs.next()) {
		    recentCustId = rs.getInt(1);
		}
		System.out.println(recentCustId);

		
		String newSubscription = "INSERT INTO Subscription(Holiday_Start, Holiday_End, Independent, MIrror, Leader, Topic, Cust_ID)VALUES('"+date_start+"', '"+date_end+"', '"+i+"', '"+m+"', '"+l+"', '"+t+"', '"+recentCustId+"')";     
        try 
        {
			stmt.executeUpdate(newSubscription);
			System.out.println("Subscription Insert Completed");
			return true;
		}
        catch (SQLException e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
        
	}
	
	
	public boolean addNewDocket(String area, String delivery_firstName, String delivery_surname, String cust_firstName, String cust_surname, int sub_id)
	{
		this.docket_delarea = area;
		this.docket_delivery_firstName = delivery_firstName;
		this.docket_delivery_surname = delivery_surname;
		this.docket_cust_firstName = cust_firstName;
		this.docket_cust_surname = cust_surname;
		this.docket_sub_id = sub_id;

        String newDocket = "INSERT INTO Dockets(Del_Area, Del_FirstName, Del_Surname, Customer_FirstName, Customer_Surname, Sub_ID) VALUES('"+docket_delarea+"', '"+docket_delivery_firstName+"', '"+docket_delivery_surname+"', '"+docket_cust_firstName+"', '"+docket_cust_surname+"', '"+docket_sub_id+"')";
        
        try 
        {
			stmt.executeUpdate(newDocket);
	        System.out.println("Delivery Person Insert Completed");
			return true;
		}
        catch (SQLException e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
}